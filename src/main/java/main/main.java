package main;

import java.util.ArrayList;
import java.util.List;

public class main {
/*
* Validating parsed String from test class
* */

    public int add(String input) {
        if (input.trim().equals("")) {
            return 0;
        } else {
            if (input.startsWith("//")) {
                String delim = input.substring(input.indexOf("//"),
                        input.indexOf("\n"))
                        .replace("//", "")
                        .replace("[", "")
                        .replace("]", "");


                String values = input.substring(input.indexOf("\n") + 1);
                String[] parser = values.split("[" + delim + "]");
                validateNums(parser);
                return addNumbers(parser);
            }

            String[] value = input.split("[,\n]");
            validateNums(value);
            return addNumbers(value);
        }
    }

    private void validateNums(String[] parser) {
        List<Integer> negatives = new ArrayList<>();
        for (String value : parser) {
            if (!value.equals("") && Integer.parseInt(value) < 0) {
                negatives.add(Integer.parseInt(value));
            }
        }
        if (!negatives.isEmpty()) {
            throw new IllegalArgumentException("negatives not allowed " + negatives);
        }
    }

    private int addNumbers(String[] input) {
        int sum = 0;
        for (String values : input) {
            if (!values.equals("") && Integer.parseInt(values) < 1000) {
                sum += Integer.parseInt(values);
            }


        }

        return sum;
    }
}
