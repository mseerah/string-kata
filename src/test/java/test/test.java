package test;
import main.main;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

//Parsing different types of String Numbers
public class test {

    //instantiate btc object for method calls
    main btc = new main();

    @Test
    public void returnZero() {
        assertEquals(0, btc.add(""));
    }

    @Test
    public void returnOne() {
        assertEquals(2, btc.add("2"));
    }

    @Test
    public void returnSumTwo() {
        assertEquals(3, btc.add("1,2"));
    }

    @Test
    public void returnUknownSum() {
        assertEquals(6, btc.add("1,2,3"));
    }

    @Test
    public void returnNewLine() {
        assertEquals(6, btc.add("1,2\n3"));
    }

    @Test
    public void returnDiffDelim() {
        assertEquals(3, btc.add("//;\n1;2"));
    }

    @Test
    public void returnDelimLength() {
        assertEquals(6, btc.add("//[***]\n1***2***3"));
    }

    @Test
    public void returnMultiDelim() {
        assertEquals(6, btc.add("//[*][%]\n1*2%3"));
    }

    @Test
    public void returnMultiDelimChars() {
        assertEquals(6, btc.add("//[---][%%%]\n1---2%%%3"));
    }

    @Test
    public void returnAboveThousand() {
        assertEquals(2, btc.add("1001,2"));
    }

    @Test
    public void returnNegative() {
        Exception ex = null;

        try {
            btc.add("1,-2,3,-4");
        } catch (Exception e) {
            ex = e;
        }
        assertEquals("negatives not allowed [-2, -4]",
                ex.getMessage());
    }
}
